import Booking from '../Booking/Booking';
import LearnMore from '../Booking/LearnMore';
import InspirationNextTrip from '../Booking/InspirationNextTrip';
import Experiences from '../Booking/Experiences';
import ShopGiftCard from '../Booking/ShopGiftCard';
import Question from '../Booking/Question';
import NavBar from '../Booking/NavBar';
import React from 'react';


class Home extends React.Component {
    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <NavBar />
                    <LearnMore />
                    <Booking />
                    <InspirationNextTrip />
                    <Experiences />
                    <ShopGiftCard />
                    <Question />
                </header>
            </div>
        );
    }
}

export default Home;
