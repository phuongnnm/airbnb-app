import React from 'react';
import logo from '../../assets/Images/bookingPicture.png';
import '../../styles/Booking.scss'

class Booking extends React.Component {
    render() {
        console.log('logooooo')
        console.log(logo)
        return (
            <>
                <div className="booking-container">
                    <div className='space' />
                    <img src={logo} className='booking-picture' alt='Booking' />
                    <div className="text-centered">Let your curiosity do the booking</div>
                    <button className='booking-button'>I'm flexible</button>
                </div>
            </>
        )
    }
}

export default Booking;