import React from "react";
import '../../styles/LearnMore.scss'

class LearnMore extends React.Component {
    render() {
        return (
            <>
                <div className='space' />
                <div className="learn-more-container">
                    <div className="learn-more-title">Help house 100,000 refugees fleeing Ukraine</div>
                    <button className='learn-more-button'>Learn more</button>
                </div>
            </>
        )
    }
}

export default LearnMore;