import React from "react";
import '../../styles/InspirationNextTrip.scss'

import pic1 from '../../assets/Images/picture1.png';
import pic2 from '../../assets/Images/picture2.png';
import pic3 from '../../assets/Images/picture3.png';
import pic4 from '../../assets/Images/picture4.png';

class InspirationNextTrip extends React.Component {
    render() {
        return (
            <>
                <div className='space' />
                <div className='next-trip-container'>
                    <div className='next-trip-title'>Inspiration for your next trip</div>
                    <div className='picture-container'>
                        <div className='pic1-container'>
                            <img className='pic1' src={pic1}></img>
                            <p className='text1a'>Ho Chi Minh City</p>
                            <p className='text1b'>9 kilometers away</p>
                        </div>
                        <div className='pic2-container'>
                            <img className='pic2' src={pic2}></img>
                            <p className='text2a'>Can Tho City</p>
                            <p className='text2b'>132 kilometers away</p>
                        </div>
                        <div className='pic3-container'>
                            <img className='pic3' src={pic3}></img>
                            <p className='text3a'>Da Lat City</p>
                            <p className='text3b'>231 kilometers away</p>
                        </div>
                        <div className='pic4-container'>
                            <img className='pic4' src={pic4}></img>
                            <p className='text4a'>Nha Trang</p>
                            <p className='text4b'>317 kilometers away</p>
                        </div>
                    </div>
                </div>
            </>

        )
    }
}

export default InspirationNextTrip;