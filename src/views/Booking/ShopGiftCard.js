import React from "react";
import '../../styles/ShopGiftCard.scss'

import giftcard from '../../assets/Images/giftcard.png';

class ShopGiftCard extends React.Component {
    render() {
        return (
            <>
                <div className='space' />
                <div className='shop-container'>
                    <img className='pic' src={giftcard}></img>
                    <p className='text'>Shop Airbnb gift cards</p>
                    <button className='button'>Learn more</button>
                </div>
            </>
        )
    }
}

export default ShopGiftCard;