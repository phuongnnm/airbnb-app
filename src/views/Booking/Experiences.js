import React from "react";
import '../../styles/Experiences.scss'

import exp1 from '../../assets/Images/exp1.png';
import exp2 from '../../assets/Images/exp2.png';

class Experiences extends React.Component {
    render() {
        return (
            <>
                <div className='space' />
                <div className='experiences-container'>
                    <div className='experiences-title'>Discover Airbnb Experiences</div>
                    <div className='picture-container'>
                        <div className='pic1-container'>
                            <img className='pic1' src={exp1}></img>
                            <p className='text1'>Things to do on your trip</p>
                            <button className='button1'>Experiences</button>
                        </div>
                        <div className='pic2-container'>
                            <img className='pic2' src={exp2}></img>
                            <p className='text2'>Things to do from home</p>
                            <button className='button2'>Online Experiences</button>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default Experiences;