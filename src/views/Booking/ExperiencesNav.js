import React from "react";
import '../../styles/ExperiencesNav.scss'

class ExperiencesNav extends React.Component {
    render() {
        return (
            <div className='button-container'>
                <button className='button1'>
                    <p className='text1'>Location</p>
                    <p className='text2'>Where are you going?</p>
                </button>
                <button className='button2'>
                    <p className='text1'>Date</p>
                    <p className='text2'>Add when you want to go</p>
                </button>
                <button className='search-button'>&#128269;</button>
            </div>
        )
    }
}

export default ExperiencesNav;