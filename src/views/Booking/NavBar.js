import React from "react";
import logo from '../logo.svg';
import ExperiencesNav from "./ExperiencesNav";
import PlacesToStayNav from "./PlacesToStayNav";
import '../../styles/NavBar.scss'
import airbnblogo from '../../assets/Images/airbnblogo.png';

class NavBar extends React.Component {
    state = {
        showPlacesNav: true,
    }

    handlePlacesButton = () => {
        this.setState({
            showPlacesNav: true
        })
    }

    handleExpButton = () => {
        this.setState({
            showPlacesNav: false
        })
    }

    render() {
        return (
            <>
                <div className='nav-container'>
                    <a href='/home'>
                        <img className='logo' src={airbnblogo} />
                    </a>
                    <button
                        className='places-button'
                        onClick={() => this.handlePlacesButton()}
                    >
                        Places to stay
                    </button>
                    <button
                        className='exp-button'
                        onClick={() => this.handleExpButton()}
                    >
                        Experiences
                    </button>
                    <a href='/onlexp'>
                        <button className='onl-exp-button'>Online Experiences</button>
                    </a>
                    <button className='host-button'>Become a Host</button>
                    <button className='lang-button'>&#127760;</button>
                    <button className='login-button'>&#8801;</button>
                </div>
                <div>
                    {
                        (this.state.showPlacesNav === true ?
                            <PlacesToStayNav />
                            : <ExperiencesNav />
                        )
                    }
                </div>
            </>
        )
    }
}

export default NavBar;