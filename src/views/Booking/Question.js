import React from 'react';
import question from '../../assets/Images/question.png';
import '../../styles/Question.scss'

class Question extends React.Component {
    render() {
        return (
            <>
                <div className='question-container'>
                    <div className='space' />
                    <img src={question} className='pic' alt='Question' />
                    <div className="text">Questions about hosting?</div>
                    <button className='button'>Ask a Superhost</button>
                </div>
            </>
        )
    }
}

export default Question;