import React from "react";
import '../../styles/PlacesToStayNav.scss'

class PlacesToStayNav extends React.Component {
    render() {
        return (
            <div className='button-container'>
                <button className='location-button'>
                    <p className='text1'>Location</p>
                    <p className='text2'>Where are you going?</p>
                </button>
                <button className='checkin-button'>
                    <p className='text1'>Check in</p>
                    <p className='text2'>Add dates</p>
                </button>
                <button className='checkout-button'>
                    <p className='text1'>Check out</p>
                    <p className='text2'>Add dates</p>
                </button>
                <button className='guests-button'>
                    <p className='text1'>Guests</p>
                    <p className='text2'>Add guests</p>
                </button>
                <button
                    className='search-button'>&#128269;</button>
            </div>
        )
    }
}

export default PlacesToStayNav;