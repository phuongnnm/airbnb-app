import './App.css';
// import Booking from './Booking/Booking';
// import LearnMore from './Booking/LearnMore';
// import InspirationNextTrip from './Booking/InspirationNextTrip';
// import Experiences from './Booking/Experiences';
// import ShopGiftCard from './Booking/ShopGiftCard';
// import Question from './Booking/Question';
// import NavBar from './Booking/NavBar';
import Home from './Home/Home';
import OnlineExperiences from './Booking/OnlineExperiences';
import NavBar from './Booking/NavBar';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Routes
} from "react-router-dom";

function App() {
  return (
    <Router>
      <header>
        <Home />

        <Routes>
          <Route path='/home' exact element={<Home />} />
          <Route path='/onlexp' exact element={<OnlineExperiences />} />
        </Routes>
      </header>
    </Router>
  );
}

export default App;
